﻿@;==============================================================================
@;
@;	"garlic_itcm_mem.s":	código de rutinas de soporte a la carga de
@;							programas en memoria (version 1.0)
@;
@;==============================================================================

.section .itcm,"ax",%progbits

	.arm
	.align 2


	.global _gm_reubicar
	@; rutina para interpretar los 'relocs' de un fichero ELF y ajustar las
	@; direcciones de memoria correspondientes a las referencias de tipo
	@; R_ARM_ABS32, restando la dirección de inicio de segmento y sumando
	@; la dirección de destino en la memoria;
	@;Parámetros:
	@; R0: dirección inicial del buffer de fichero (char *fileBuf)
	@; R1: dirección de inicio de segmento (unsigned int pAddr)
	@; R2: dirección de destino en la memoria (unsigned int *dest)
	@;Resultado:
	@; cambio de las direcciones de memoria que se tienen que ajustar
_gm_reubicar:
	push {r3-r12, lr}
	
	@; R0 = dirección inicial del buffer de fichero (bufferFichero)
	@; R1 = dirección de inicio de segmento
	@; R2 = dirección de destino en la memoria
	@; acceder a las secciones de reubicadores, dentro del buffer del fichero ELF
	
	ldr r3, [r0, #32]		@; cargar e_shoff (desplazamiento de la tabla de secciones)
	ldrb r8, [r0, #46]		@; cargar e_shentsize (tamaño de cada entrada de la tabla de secciones)
	ldrb r12, [r0, #48]		@; cargar e_shnum (número de entradas de la tabla de secciones)
	mov r11, #0				@; inicializar a 0 el contador del número de secciones
	
	
.L_inicio_seccion:
	add r4, r3, #4			@; sumar 4 al desplazamiento de la tabla de secciones para obtener el tipo de la sección sh_type (saltar el sh_name)
	ldr r5, [r0, r4]		@; cargar el tipo de la sección sh_type
	cmp r5, #9				@; comparar el tipo con 9 (las secciones de reubicadores son del tipo 9 (SHT_REL))
	beq .L_inicio_reubicar	@; si la sección es de tipo 9 es una sección de reubicación
	bne .L_siguiente_seccion	@; si no, comprobar la siguiente sección
	
.L_inicio_reubicar:
	push {r11, r12}
	
	add r4, r3, #16			@; sumar 16 a e_shoff
	ldr r6, [r0, r4]		@; cargar sh_offset (inicio tabla reubicadores)
	add r4, r3, #20			@; sumar 20 a e_shoff
	ldr r11, [r0, r4]		@; cargar sh_size (tamaño de la sección)
	add r4, r3, #36			@; sumar 36 a e_shoff
	ldr r12, [r0, r4]		@; cargar sh_entsize (tipo SHT_REL, tamaño en bytes de cada reubicador)
	
.L_reubicar:
	add r7, r6, #4
	ldrb r7, [r0, r7]		@; cargar info (little endian)
	cmp r7, #2				@; comparar el tipo con 2 (info acaba en 02)
	bne .L_siguiente_reubicador	@; si NO es de código numérico 2 pasar al siguiente reubicador
	
.L_cambiar:					@; si es de código numérico 2 (R_ARM_ABS32) es necesaria reubicación
	ldr r9, [r0, r6]		@; cargar el offset del reubicador
	add r9, r9, r2			@; sumar la dirección de destino en la memoria (+1002000)
	sub r9, r9, r1			@; restar la dirección de inicio de segmento (-8000)
	ldr r10, [r9]			@; cargar el valor de la dirección destino a corregir
	add r10, r10, r2		@; sumar la dirección de destino en la memoria (+1002000)
	sub r10, r10, r1		@; restar la dirección de inicio de segmento (-8000)
	str r10, [r9]			@; guardar el valor del nuevo offset
	str r9, [r0, r6]		@; guardar la dirección destino en el offset del reubicador
	
.L_siguiente_reubicador:
	add r6, r6, r12			@; calcular inicio del siguiente reubicador
	sub r11, r11, r12		@; resta sh_entsize al sh_size
	cmp r11, #0				@; comparar el contador con 0 para comprovar si llega al final de la sección
	beq .L_sig_secc_2		@; saltar a la siguiente sección
	b .L_reubicar
	
.L_sig_secc_2:
	pop {r11, r12}
	b .L_siguiente_seccion

.L_siguiente_seccion:
	add r11, r11, #1		@; incrementar en 1 el contador del número de secciones
	cmp r11, r12			@; comparar si ha llegado al final de las secciones
	beq .L_final
	add r3, r3, r8			@; sumar tamaño entrada de la tabla de secciones
	b .L_inicio_seccion

.L_final:

	pop {r3-r12, pc}


.end

