/*------------------------------------------------------------------------------

	"garlic_mem.c" : fase 1 / programador M

	Funciones de carga de un fichero ejecutable en formato ELF, para GARLIC 1.0

------------------------------------------------------------------------------*/
#include <nds.h>
#include <filesystem.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <garlic_system.h>	/* definición de funciones y variables de sistema */

#define EI_NIDENT 16
#define PT_LOAD 1	/* tipo de segmento */

int mem;

typedef uint32_t	Elf32_Addr;	/* dirección de memoria */
typedef uint16_t	Elf32_Half;	/* medio entero (sin signo) */
typedef uint32_t 	Elf32_Off;	/* desplazamiento dentro del fichero (sin signo) */
typedef int32_t		Elf32_Sword;/* entero con signo */
typedef uint32_t 	Elf32_Word;	/* entero (sin signo) */

/* ELF header */
typedef struct {
	unsigned char e_ident[EI_NIDENT];	/* identificación del fichero */
	Elf32_Half	e_type;			/* tipo del fichero */
	Elf32_Half 	e_machine;		/* arquitectura de la máquina */
	Elf32_Word 	e_version;		/* versión del formato ELF */
	Elf32_Addr 	e_entry;		/* punto de entrada del programa */
	Elf32_Off 	e_phoff;		/* desplazamiento de la tabla de segmentos (program header) */
	Elf32_Off 	e_shoff;		/* desplazamiento de la tabla de secciones (section header) */
	Elf32_Word 	e_flags;		/* flags específicas de la arquitectura*/
	Elf32_Half 	e_ehsize;		/* tamaño de la cabecera del ELF en bytes */
	Elf32_Half 	e_phentsize;	/* tamaño de cada entrada de la tabla de segmentos */
	Elf32_Half 	e_phnum;		/* número de entradas de la tabla de segmentos */
	Elf32_Half 	e_shentsize;	/* tamaño de cada entrada de la tabla de secciones */
	Elf32_Half 	e_shnum;		/* número de entradas de la tabla de secciones */
	Elf32_Half 	e_shstrndx;		/* nombre de la sección de strings sección */
} Elf32_Ehdr;

/* Program header */
typedef struct {
	Elf32_Word	p_type;		/* tipo del segmento */
	Elf32_Off	p_offset;	/* desplazamiento en el fichero del primer byte del segmento */
	Elf32_Addr	p_vaddr;	/* dirección virtual en imagen de memoria */
	Elf32_Addr	p_paddr;	/* dirección física donde se tendría que cargar el segmento */
	Elf32_Word	p_filesz;	/* tamaño del segmento dentro del fichero */
	Elf32_Word	p_memsz;	/* tamaño del segmento dentro de la memoria */
	Elf32_Word	p_flags;	/* flags del permiso de acceso, lectura (R), escritura (W), ejecutable (E) o combinación */
	Elf32_Word	p_align;	/* alineación en memoria y fichero */
} Elf32_Phdr;

/* Section header */
typedef struct {
	Elf32_Word 	sh_name;		
	Elf32_Word 	sh_type;	/* tipo de la sección */ // las secciones de reubicadores son del tipo 9 (SHT_REL)
	Elf32_Word 	sh_flags;
	Elf32_Addr 	sh_addr;
	Elf32_Off 	sh_offset;	/* desplazamiento en el fichero del primer byte de la sección */
	Elf32_Word 	sh_size;	/* tamaño de la sección dentro del fichero */
	Elf32_Word 	sh_link;	/* para sección de tipo SHT_REL, indica el índice de la sección que contiene la tabla de símbolos asociada a los reubicadores */
	Elf32_Word 	sh_info;	/* para sección de tipo SHT_REL, indica el índice de la sección sobre la cual se deberán aplicar los reubicadores */
	Elf32_Word 	sh_addralign;
	Elf32_Word 	sh_entsize;	/* para sección de tipo SHT_REL, indica el tamaño en bytes de cada reubicador (típicamente, 8 bytes) */
} Elf32_Shdr;

/* Estructura de los reubicadores */
typedef struct {
	Elf32_Addr 	r_offset;	/* dirección de memoria virtual (o física) sobre la que hay que aplicar la reubicación */
	Elf32_Word 	r_info;		/* los 8 bits bajos contienen el tipo de reubicación que hay que aplicar, y el resto indican el índice del símbolo sobre el que se aplicará la reubicación */
} Elf32_Rel;


/* _gm_initFS: inicializa el sistema de ficheros, devolviendo un valor booleano
					para indiciar si dicha inicialización ha tenido éxito; */
int _gm_initFS()
{
	return nitroFSInit(NULL);	/* inicializar sistema de ficheros NITRO */
}



/* _gm_cargarPrograma: busca un fichero de nombre "(keyName).elf" dentro del
				directorio "/Programas/" del sistema de ficheros, y carga los
				segmentos de programa a partir de una posición de memoria libre,
				efectuando la reubicación de las referencias a los símbolos del
				programa, según el desplazamiento del código y los datos en la
				memoria destino;
	Parámetros:
		zocalo	->	índice del zócalo que indexará el proceso del programa
		keyName ->	vector de 4 caracteres con el nombre en clave del programa
	Resultado:
	,   != 0	->	dirección de inicio del programa (intFunc)
		== 0	->	no se ha podido cargar el programa
*/
intFunc _gm_cargarPrograma(int zocalo, char *keyName)
{
	int tamanoFichero, i;
	unsigned int direccionInicioPrograma = 0;
	char nombreFichero[9], path[20], *bufferFichero;
	FILE* fichero;
	Elf32_Ehdr *cabeceraELF;
	Elf32_Phdr *cabeceraSegmento;
	
	
	/*	Buscar el fichero "(keyname).elf" en el directorio Programas del
		sistema de ficheros Nitro, contenido dentro del sistema operativo,
		donde "(keyname)" será el nombre en clave del programa (por
		convenio, solo cuatro carácteres en mayúsculas) */
	
	strcpy(nombreFichero, keyName);
	strcat(nombreFichero, ".elf");
	strcpy(path, "/Programas/");
	strcat(path, nombreFichero);	
	
	fichero = fopen(path,"rb");	/* Abrir el fichero ELF en modo binario */
	
	if (fichero == NULL)
	{
		printf("No se ha podido abrir el fichero.\n");
	}
	else
	{			
		fseek(fichero, 0, SEEK_END);		/* fseek establece la posición del archivo de la secuencia en el desplazamiento indicado */
		tamanoFichero = ftell(fichero);		/* ftell devuelve la posición actual del archivo de la transmisión dada */
		fseek(fichero, 0, SEEK_SET);		/* fseek establece la posición del archivo de la secuencia en el desplazamiento indicado */
		//printf("Tamano de '%s': %i bytes\n", nombreFichero, tamanoFichero);
		
		/*	Si encuentra el fichero, cargarlo íntegramente dentro de un buffer
			de memoria dinámica, que permitirá acceder a su contenido en los
			siguientes pasos del algoritmo de forma más eficiente */
		bufferFichero = (char*) malloc(tamanoFichero);	/* reservar espacio en un buffer de memoria dinámica */
		if (!bufferFichero)
		{
			printf("Memoria insuficiente.\n");
		}
		else
		{
			if (fread(bufferFichero, 1, tamanoFichero, fichero) != tamanoFichero)	/* fread lee el contenido del fichero */
				printf("Error al leer los bytes del fichero.\n");
			else
			{				
				/*	Acceder a la cabecera ELF para obtener la posición (offset)
					y el tamaño de la tabla de segmentos */
				cabeceraELF = (Elf32_Ehdr *) bufferFichero;
				
				/*	Acceder a la tabla de segmentos; para cada segmento de tipo PT_LOAD: */
				for (i = 0; i < (int)cabeceraELF->e_phnum; i++)
				{
					/* 	Obtener la dirección de memoria inicial del segmento a cargar (campo p_paddr),
						así como el desplazamiento dentro del fichero donde empieza el segmento (campo p_offset) */
					cabeceraSegmento = 	(Elf32_Phdr *) &bufferFichero[cabeceraELF->e_phoff + i * (int)cabeceraELF->e_phentsize];
					
					if (cabeceraSegmento->p_type == PT_LOAD)	/* si el segmento es de tipo PT_LOAD */
					{
						flagsSegmento = cabeceraSegmento->p_flags;	/* Obtener el valor del campo p_flags de cada entrada de la tabla de segmentos */

						if (flagsSegmento == 5)	/* Si el campo p_flags es 5 (R-E) es un segmento de código */
						{
							paddrSegmentoCodigo = cabeceraSegmento->p_paddr;		/* Obtener la dirección física donde se tendría que cargar el segmento */
							offsetSegmentoCodigo = cabeceraSegmento->p_offset;	/* Obtener el desplazamiento en el fichero del primer byte del segmento */
							fileszSegmentoCodigo = cabeceraSegmento->p_filesz;	/* Obtener el tamaño del segmento dentro del fichero */
							memszSegmentoCodigo = cabeceraSegmento->p_memsz;		/* Obtener el tamaño del segmento dentro de la memoria */
							
							/* Cargar el contenido del segmento a partir de una dirección de memoria destino apropiada */
							direccionReservaCodigo = (intFunc)_gm_reservarMem(zocalo, cabeceraSegmento->p_memsz, 0);
							
							if (direccionReservaCodigo == 0)
							{
								printf("No queda un espacio de memoria consecutivo del tamaño requerido.\n");
								return 0;
							}
							
							_gs_copiaMem(&bufferFichero[cabeceraSegmento->p_offset], (void *)direccionReservaCodigo, cabeceraSegmento->p_filesz);
						}
						else if (flagsSegmento == 6)	/* Si el campo p_flags es 6 (R-E) es un segmento de datos */
						{
							paddrSegmentoDatos = cabeceraSegmento->p_paddr;		/* Obtener la dirección física donde se tendría que cargar el segmento */
							offsetSegmentoDatos = cabeceraSegmento->p_offset;	/* Obtener el desplazamiento en el fichero del primer byte del segmento */
							fileszSegmentoDatos = cabeceraSegmento->p_filesz;	/* Obtener el tamaño del segmento dentro del fichero */
							memszSegmentoDatos = cabeceraSegmento->p_memsz;		/* Obtener el tamaño del segmento dentro de la memoria */

							/* Cargar el contenido del segmento a partir de una dirección de memoria destino apropiada */
							direccionReservaDatos = (intFunc)_gm_reservarMem(zocalo, cabeceraSegmento->p_memsz, 1);
							
							/* Si existe memoria para el primer segmento (segmento de código) pero no para el segundo (segmento de datos) */
							if (direccionReservaCodigo != 0 && direccionReservaDatos == 0) {
								_gm_liberarMem(zocalo);	/* Liberar la memoria asignada al primer segmento al no poder cargar todo el programa íntegro */
							}

							if (direccionReservaDatos == 0)
							{
								printf("No queda un espacio de memoria consecutivo del tamaño requerido.\n");
								return 0;
							}
							
							_gs_copiaMem(&bufferFichero[cabeceraSegmento->p_offset], (void *)direccionReservaDatos, cabeceraSegmento->p_filesz);
						}

						printf("> segmento de programa copiado:\n");			
					}
				}

				/* 	Efectuar la reubicacón de todas las posiciones sensibles a la dirección
					del código en memoria, invocando a la rutina _gm_reubicar() */
				_gm_reubicar(bufferFichero, (unsigned int) paddrSegmentoCodigo, (unsigned int *) paddrSegmentoDatos, (unsigned int) direccionReservaCodigo, (unsigned int*) direccionReservaDatos);
				//printf("Final reubicacion\n");
				
				/* Devolver la dirección de inicio del programa (intFunc) */
				direccionInicioPrograma = cabeceraELF->e_entry - paddrSegmentoCodigo + (int) iniciMemCode;

				free(bufferFichero);	/* free desasigna la memoria previamente asignada por una llamada a malloc */
				fclose(fichero);
				
				/* 	Si todo el proceso ha funcionado correctamente, devolver la dirección de inicio del programa
					para el segmento que contenga el punto de entrada e_entry, convenientemente reubicada */
				return ((intFunc) direccionInicioPrograma);
			}
		}
	}
	
	fclose(fichero);
	
	/*	En caso de que se haya detectado algún inconveniente, devolver 0 */
	return ((intFunc) 0);
}