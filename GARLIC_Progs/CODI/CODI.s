	.arch armv5te
	.eabi_attribute 23, 1
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 0
	.eabi_attribute 18, 4
	.file	"CODI.c"
	.section	.rodata
	.align	2
.LC0:
	.ascii	"-- Programa CODI --\012\000"
	.align	2
.LC1:
	.ascii	"(%j)\011C\363digo barras con d\355gito control: \012"
	.ascii	"\011\000"
	.align	2
.LC2:
	.ascii	"%i\000"
	.align	2
.LC3:
	.ascii	"%i\012\000"
	.text
	.align	2
	.global	_start
	.syntax unified
	.arm
	.fpu softvfp
	.type	_start, %function
_start:
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r4, fp, lr}
	add	fp, sp, #8
	sub	sp, sp, #52
	str	r0, [fp, #-56]
	mov	r3, sp
	mov	r4, r3
	mov	r3, #0
	str	r3, [fp, #-32]
	mov	r3, #0
	str	r3, [fp, #-36]
	ldr	r3, [fp, #-56]
	cmp	r3, #0
	bge	.L2
	mov	r3, #0
	str	r3, [fp, #-56]
	b	.L3
.L2:
	ldr	r3, [fp, #-56]
	cmp	r3, #3
	ble	.L3
	mov	r3, #3
	str	r3, [fp, #-56]
.L3:
	bl	GARLIC_pid
	mov	r3, r0
	mov	r1, r3
	ldr	r0, .L15
	bl	GARLIC_printf
	ldr	r3, [fp, #-56]
	add	r2, r3, #1
	mov	r3, r2
	lsl	r3, r3, #1
	add	r3, r3, r2
	lsl	r3, r3, #1
	str	r3, [fp, #-40]
	ldr	ip, [fp, #-40]
	mov	r3, ip
	sub	r3, r3, #1
	str	r3, [fp, #-44]
	mov	r0, ip
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	lsl	r3, r1, #5
	orr	r3, r3, r0, lsr #27
	lsl	r2, r0, #5
	mov	r0, ip
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	lsl	r3, r1, #5
	orr	r3, r3, r0, lsr #27
	lsl	r2, r0, #5
	lsl	r3, ip, #2
	add	r3, r3, #3
	add	r3, r3, #7
	lsr	r3, r3, #3
	lsl	r3, r3, #3
	sub	sp, sp, r3
	mov	r3, sp
	add	r3, r3, #3
	lsr	r3, r3, #2
	lsl	r3, r3, #2
	str	r3, [fp, #-48]
	mov	r3, #0
	str	r3, [fp, #-24]
	b	.L4
.L13:
	mov	r3, #0
	str	r3, [fp, #-16]
	mov	r3, #0
	str	r3, [fp, #-28]
	mov	r3, #0
	str	r3, [fp, #-36]
	ldr	r1, [fp, #-24]
	ldr	r0, .L15+4
	bl	GARLIC_printf
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L5
.L6:
	bl	GARLIC_random
	mov	r3, r0
	str	r3, [fp, #-52]
	ldr	r3, [fp, #-52]
	cmp	r3, #9
	bhi	.L6
	ldr	r1, [fp, #-52]
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-20]
	str	r1, [r3, r2, lsl #2]
	ldr	r1, [fp, #-52]
	ldr	r0, .L15+8
	bl	GARLIC_printf
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L5:
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-40]
	cmp	r2, r3
	bcc	.L6
	mov	r3, #0
	str	r3, [fp, #-20]
	b	.L7
.L10:
	ldr	r3, [fp, #-20]
	and	r3, r3, #1
	cmp	r3, #0
	bne	.L8
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-20]
	ldr	r3, [r3, r2, lsl #2]
	mov	r2, r3
	ldr	r3, [fp, #-16]
	add	r3, r3, r2
	str	r3, [fp, #-16]
	b	.L9
.L8:
	ldr	r3, [fp, #-48]
	ldr	r2, [fp, #-20]
	ldr	r2, [r3, r2, lsl #2]
	mov	r3, r2
	lsl	r3, r3, #1
	add	r3, r3, r2
	mov	r2, r3
	ldr	r3, [fp, #-28]
	add	r3, r3, r2
	str	r3, [fp, #-28]
.L9:
	ldr	r3, [fp, #-20]
	add	r3, r3, #1
	str	r3, [fp, #-20]
.L7:
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-40]
	cmp	r2, r3
	bcc	.L10
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-28]
	add	r3, r2, r3
	str	r3, [fp, #-32]
	b	.L11
.L12:
	ldr	r3, [fp, #-36]
	add	r3, r3, #1
	str	r3, [fp, #-36]
	ldr	r3, [fp, #-32]
	add	r3, r3, #1
	str	r3, [fp, #-32]
.L11:
	ldr	r1, [fp, #-32]
	ldr	r0, .L15+12
	umull	r2, r3, r1, r0
	lsr	r2, r3, #3
	mov	r3, r2
	lsl	r3, r3, #2
	add	r3, r3, r2
	lsl	r3, r3, #1
	sub	r2, r1, r3
	cmp	r2, #0
	bne	.L12
	ldr	r1, [fp, #-36]
	ldr	r0, .L15+16
	bl	GARLIC_printf
	ldr	r3, [fp, #-24]
	add	r3, r3, #1
	str	r3, [fp, #-24]
.L4:
	ldr	r3, [fp, #-24]
	cmp	r3, #19
	bls	.L13
	mov	r3, #0
	mov	sp, r4
	mov	r0, r3
	sub	sp, fp, #8
	@ sp needed
	pop	{r4, fp, pc}
.L16:
	.align	2
.L15:
	.word	.LC0
	.word	.LC1
	.word	.LC2
	.word	-858993459
	.word	.LC3
	.size	_start, .-_start
	.ident	"GCC: (devkitARM release 47) 7.1.0"
