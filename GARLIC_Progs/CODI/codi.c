/*------------------------------------------------------------------------------

	"CODI.c" : programa de usuario para el sistema operativo GARLIC 1.0;
	
	Calcula el d�gito de control para 20 c�digos de barras aleatorios
	generados de longitud (arg+1)*6, d�nde arg es el argumento ([0..3]).

------------------------------------------------------------------------------*/

#include <GARLIC_API.h>			/* definici�n de las funciones API de GARLIC */

#define NUM_CODIGOS 20			/* n�mero de c�digos a generar */

int _start(int arg)				/* funci�n de inicio : no se usa 'main' */
{
	unsigned int longitud, i, j, numero_aleatorio, suma_pares, suma_impares, suma_total = 0, digito_control = 0;
		
	if (arg < 0) arg = 0;			// limitar valor m�ximo y 
	else if (arg > 3) arg = 3;		// valor m�nimo del argumento
	
									// escribir mensaje inicial
	GARLIC_printf("-- Programa CODI --\n", GARLIC_pid());
	
	longitud = (arg + 1) * 6;
	
	int codigo_barras[longitud];
	
	for (i = 0; i < NUM_CODIGOS; i++)
	{
		suma_pares = 0;
		suma_impares = 0;
		digito_control = 0;
		
		GARLIC_printf("(%j)\tC�digo barras con d�gito control: \n\t", i);	// escribir mensaje
		
		for (j = 0; j < longitud; j++)
		{
			do
			{
				numero_aleatorio = GARLIC_random();	//  genera un d�gito del n�mero del c�digo de barras de manera aleatoria
			} while (numero_aleatorio<0 || numero_aleatorio>9);
			
			codigo_barras[j] = numero_aleatorio;
			GARLIC_printf("%i", numero_aleatorio);	// escribir mensaje
		}
		
		for (j = 0; j < longitud; j++)
		{
			if (j % 2 == 0)
				suma_pares = suma_pares + codigo_barras[j] * 1;	// multiplica por 1 los d�gitos en posici�n par y los suma
			else
				suma_impares = suma_impares + codigo_barras[j] * 3;	// multiplica por 3 los d�gitos en posici�n impar y los suma
		}
		
		suma_total = suma_pares + suma_impares;	// suma de los sumas obtenidas
		while (suma_total % 10 != 0)	// se busca la decena superior al resultado de la suma anterior
		{
			digito_control++;	// d�gito de control del c�digo de barras (diferencia entre la decena superior buscada y la suma total)
			suma_total++;
		}
		
		GARLIC_printf("%i\n", digito_control);	// escribir mensaje
	}
	
	return 0;
}